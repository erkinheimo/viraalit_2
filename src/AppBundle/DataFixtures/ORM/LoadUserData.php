<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 30.4.2017
 * Time: 11:43
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{


    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setEmail('');
        $user->setUsername('admin');

        $user->setPlainPassword('qwerty12345roope');

        $user->addRole('ROLE_ADMIN');
        $user->addRole('ROLE_USER');

        $user->setEnabled(true);


        $manager->persist($user);
        $manager->flush();

    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 302;
    }

}