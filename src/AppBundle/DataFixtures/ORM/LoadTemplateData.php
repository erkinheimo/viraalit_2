<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 22.4.2017
 * Time: 20:02
 */

namespace AppBundle\DataFixtures\ORM;


use Symfony\Component\Finder\Finder;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\ORM\EntityRepository;

use AppBundle\Entity\Image;
use AppBundle\Entity\Template;


class LoadTemplateData implements FixtureInterface {



    public function load(ObjectManager $manager)
    {

        /** @var EntityRepository $em */
        $em = $manager->getRepository('AppBundle:Image');

        $finder = new Finder();
        $finder->files()->in('src/AppBundle/Data/Images');

        foreach ($finder as $file) {
            $image = $em->findOneBy(array('imageName' => $file->getFilename()));


            if($image) {
                $template = new Template();
                $template->setImage($image);

                $manager->persist($template);
            }
        }
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 300;
    }

}