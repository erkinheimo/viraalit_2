<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 22.4.2017
 * Time: 18:56
 *
 *
 * Usage
 * php app/console doctrine:fixtures:load
 */

namespace AppBundle\DataFixtures\ORM;


use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Finder\SplFileInfo;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Image;




class LoadImageData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $path = 'src/AppBundle/Data/Images/';

        $finder = new Finder();
        $finder->files()->in($path);

        foreach ($finder as $file) {
            /** @var SplFileInfo $file */

            $image = new Image();
            $image->setImageName($file->getFilename());
            $image->setImageFile(new File($file->getRealPath()));
            $image->setRelativePath($path);

            $manager->persist($image);

        }

        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 100;
    }
}

