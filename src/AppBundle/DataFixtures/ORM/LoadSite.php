<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 2.5.2017
 * Time: 17:55
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Site;

class LoadSite implements FixtureInterface {

    private $analytics = <<<'EOD'
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                ga('create', 'UA-96774437-1', 'auto');
                ga('send', 'pageview');
            </script>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <script>
              (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-3668318095343962",
                enable_page_level_ads: true
              });
            </script>
EOD;

    public function load(ObjectManager $manager)
    {

        $site = new Site();

        $site->setTitle("Viraalit - Luo oma meemi");
        $site->setCookieconsentmessage("Tämä nettisivu käyttää evästeitä / This website uses cookies. ");
        $site->setCookieconsentlink("https://www.viestintavirasto.fi/kyberturvallisuus/palveluidenturvallinenkaytto/evasteet.html");
        $site->setCookieconsentdismiss("Got it!");
        $site->setCoonkieconsentlearnmore("Lisää tietoa viestintaviraston sivuilta.");
        $site->setGoogleanalytics($this->analytics);



        $manager->persist($site);

        $manager->flush();
    }



    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 333;
    }

}