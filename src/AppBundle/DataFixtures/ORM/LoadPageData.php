<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 28.4.2017
 * Time: 18:02
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Page;



class LoadPageData implements FixtureInterface {


    public function load(ObjectManager $manager)
    {
        $pages_data = $this->getData();

        foreach($pages_data as $data) {
            $page = new Page();
            $page->setContent($data['content']);
            $page->setName($data['name']);
            $page->setPublic($data['public']);

            $manager->persist($page);
        }
        $manager->flush();
    }


    public function getData(){
        return array(
            array(
                'name' => 'index',
                'content' => '<h1 style="text-align:center">Aloita meemin luominen kuvaa klikkaamalla.</h1>',
                'public' => true,
            ),
            array(
                'name' => 'meme_view_add_block',
                'content' => '<p> Random content stuff for adds.. Editable content. </p>',
                'public' => false,
            ),
            array(
                'name' => 'top_meme_sites',
                'content' => '<p> top meme sites... editable content </p>',
                'public' => true,
            ),
            array(
                'name' => 'about',
                'content' => '<p> about / info page </p>',
                'public' => false,
            ),
        );
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 301;
    }

}