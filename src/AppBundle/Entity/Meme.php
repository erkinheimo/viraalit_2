<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 22.4.2017
 * Time: 21:45
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Functions as func;
use AppBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemeRepository")
 * @Vich\Uploadable
 */
class Meme {

    function __construct(){
        $this->setViewcount(0);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist"})
     *
     * @Groups({"api"})
     */
    private $image;

    /** @ORM\Column(name="toptext", type="string", nullable=true)
     *
     * @Groups({"api"})
     **/
    protected $toptext;

    /** @ORM\Column(name="bottomtext", type="string", nullable=true)
     *
     * @Groups({"api"})
     **/
    protected $bottomtext;

    /** @ORM\Column(name="url", type="string")
     *
     * @Groups({"api"})
     **/
    protected $url;

    /** @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"api"})
     **/
    protected $viewcount;



    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // If no file means we are using template and image isin't added yet to meme.
        if(!$this->getImage()->getImageFile()){
            return;
        }
        // do your own validation
        if (! in_array($this->getImage()->getImageFile()->getMimeType(), array(
            'image/jpeg',
            'image/png'
        ))) {
            $context
                ->buildViolation(' Väärä kuva muoto, vain (jpg,png) sallittu. Wrong file type (only jpg,png allowed).')
                ->atPath('image')
                ->addViolation();
        }
    }

    public function createUrl()
    {
        $this->url = $this->idGen($this->toptext . $this->bottomtext . (string)$this->id) . (string)$this->getUpdatedAt()->getTimestamp();
//        $this->url = $this->idGen($this->toptext . $this->bottomtext . $this->getImage()->getImageName() . (string)$this->id);
    }

    private function idGen($str){
        $chars = str_split($str);
        $i = 0;
        $rand_int = 0;
        foreach($chars as $char){
            $i++;
            $rand_int += ord($char);
            if($i > 100){
                break;
            }
        }
        mt_srand($rand_int);
        return mt_rand(100000,PHP_INT_MAX);
    }

    public function addView()
    {
        $this->viewcount += 1;
    }

    /**
     * @return mixed
     */
    public function getViewcount()
    {
        return $this->viewcount;
    }

    /**
     * @param mixed $viewcount
     */
    public function setViewcount($viewcount)
    {
        $this->viewcount = $viewcount;
    }




    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;

        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getToptext()
    {
        return $this->toptext;
    }

    /**
     * @param mixed $toptext
     */
    public function setToptext($toptext)
    {
        $this->toptext = $toptext;

        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getBottomtext()
    {
        return $this->bottomtext;
    }

    /**
     * @param mixed $bottomtext
     */
    public function setBottomtext($bottomtext)
    {
        $this->bottomtext = $bottomtext;
    }





}