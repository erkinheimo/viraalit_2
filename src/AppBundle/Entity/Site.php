<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 *
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SiteRepository")
 */
class Site
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cookieconsentmessage", type="string", nullable=true)
     */
    private $cookieconsentmessage;

    /**
     * @var string
     *
     * @ORM\Column(name="coonkieconsentlearnmore", type="string", nullable=true)
     */
    private $coonkieconsentlearnmore;

    /**
     * @var string
     *
     * @ORM\Column(name="cookieconsentlink", type="string", nullable=true)
     */
    private $cookieconsentlink;

    /**
     * @var string
     *
     * @ORM\Column(name="cookieconsentdismiss", type="string", nullable=true)
     */
    private $cookieconsentdismiss;

    /**
     * @var string
     *
     * @ORM\Column(name="googleanalytics", type="string", nullable=true)
     */
    private $googleanalytics;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return string
     */
    public function getGoogleanalytics()
    {

        return $this->googleanalytics;
    }
    /**
     * @param string $googleanalytics
     */
    public function setGoogleanalytics($googleanalytics)
    {

        $this->googleanalytics = $googleanalytics;
    }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Site
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set cookieconsentmessage
     *
     * @param string $cookieconsentmessage
     *
     * @return Site
     */
    public function setCookieconsentmessage($cookieconsentmessage)
    {
        $this->cookieconsentmessage = $cookieconsentmessage;

        return $this;
    }

    /**
     * Get cookieconsentmessage
     *
     * @return string
     */
    public function getCookieconsentmessage()
    {
        return $this->cookieconsentmessage;
    }

    /**
     * Set coonkieconsentlearnmore
     *
     * @param string $coonkieconsentlearnmore
     *
     * @return Site
     */
    public function setCoonkieconsentlearnmore($coonkieconsentlearnmore)
    {
        $this->coonkieconsentlearnmore = $coonkieconsentlearnmore;

        return $this;
    }

    /**
     * Get coonkieconsentlearnmore
     *
     * @return string
     */
    public function getCoonkieconsentlearnmore()
    {
        return $this->coonkieconsentlearnmore;
    }

    /**
     * Set cookieconsentlink
     *
     * @param string $cookieconsentlink
     *
     * @return Site
     */
    public function setCookieconsentlink($cookieconsentlink)
    {
        $this->cookieconsentlink = $cookieconsentlink;

        return $this;
    }

    /**
     * Get cookieconsentlink
     *
     * @return string
     */
    public function getCookieconsentlink()
    {
        return $this->cookieconsentlink;
    }

    /**
     * Set cookieconsentdismiss
     *
     * @param string $cookieconsentdismiss
     *
     * @return Site
     */
    public function setCookieconsentdismiss($cookieconsentdismiss)
    {
        $this->cookieconsentdismiss = $cookieconsentdismiss;

        return $this;
    }

    /**
     * Get cookieconsentdismiss
     *
     * @return string
     */
    public function getCookieconsentdismiss()
    {
        return $this->cookieconsentdismiss;
    }
}

