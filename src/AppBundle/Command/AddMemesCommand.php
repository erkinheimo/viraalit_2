<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Meme;
use AppBundle\Entity\Template;
use AppBundle\Repository\TemplateRepository;

class AddMemesCommand extends ContainerAwareCommand
{
    /** @var EntityManager $em */
    private $em;
    /** @var  Image[] $images */
    private $images;

    protected function configure() {
        $this
            ->setName('app:generate:memes')
            ->setDescription('Generate memes for dev usage')
            ->addArgument('count', InputArgument::OPTIONAL, 'How many')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->images = $this->em->getRepository('AppBundle:Image')->findAll();

        $count = $input->getArgument('count');

        $i = 0;

        while ($i < $count) {

            $meme = $this->createMeme();

            $this->em->persist($meme);
            $i++;
        }

        $this->em->flush();

        $output->writeln($i.' memes created for you.');
    }

    protected function createMeme() {
        $meme = new Meme();
        $meme->setToptext($this->randomText());
        $meme->setBottomtext($this->randomText());
        $meme->setViewcount(random_int(1, 100));
        $meme->setImage($this->randomImage());
        $meme->createUrl();

        return $meme;
    }

    protected function randomText(){
        return bin2hex(random_bytes(10));
    }

    protected function randomImage() {

        $max = count($this->images) - 1;

        return $this->images[random_int(0,$max)];

    }

}
