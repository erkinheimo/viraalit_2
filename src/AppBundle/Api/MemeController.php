<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 23.5.2017
 * Time: 12:46
 */

namespace AppBundle\Api;


use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;

use Doctrine\ORM\EntityManager;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class MemeController  extends FOSRestController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get meme by ID",
     * )
     *
     * @Annotations\QueryParam(name="id", requirements="\d+", nullable=true, description="Memes id, integer")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function getMemeAction(ParamFetcherInterface $paramFetcher){
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $id = $paramFetcher->get('id');
        if(!$id){
            $data = array(
                'image_path'        => '/media/cache/resolve/meme/',
                'image_list_path'   => '/media/cache/resolve/list_thumb/',
                'meme_path'         => '/meme/show/',
                'memes' =>
                    array(
                        'id' => 1,
                        'updatedAt' => '2017-05-24T18:32:43+03:00',
                        'image' =>
                            array(
                                'imageName' => 'Shut-Up-And-Take-My-Money-Fry.jpg',
                                'id' => 7,
                                'updatedAt' => '2017-05-24T18:32:29+03:00',
                            ),
                        'toptext' => 'This is example',
                        'bottomtext' => 'use the id attribute',
                        'url' => '55540803852367694401495639963',
                        'viewcount' => 44,
                    ),
            );
            return $this->view($data, 200);
        }

        // query meme
        $meme = $em->getRepository('AppBundle:Meme')->findOneBy(array('id'=>$id));
        // set image

        $data = array(
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'meme_path'         => '/meme/show/',
            'memes'             => $meme,
        );

        // return meme
        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get newest memes",
     * )
     *
     * @Annotations\QueryParam(
     *      name="offset", requirements="\d+", nullable=true,
     *      description="List query will only return 100 memes at time."
     * )
     * @Annotations\QueryParam(
     *      name="order", requirements="\w+", nullable=true,
     *      description="Order by ASC or DESC, default DESC"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function listMemesAction(ParamFetcherInterface $paramFetcher){

        $em = $this->getDoctrine()->getManager();
        $offset = $paramFetcher->get('offset');
        $order = $paramFetcher->get('order');

        if(!$offset) {
            $offset = 0;
        }

        if($order && $order == 'ASC') {
            $memes = $em->getRepository('AppBundle:Meme')->findMemes(100,$offset);
        } else {
            $memes = $em->getRepository('AppBundle:Meme')->findLatest(100,$offset);
        }


        $data = array(
            'count'             => count($memes),
            'offset'            => (int)$offset,
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'meme_path'         => '/meme/show/',
            'memes'             => $memes,
        );


        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get popular memes",
     * )
     *
     * @Annotations\QueryParam(
     *      name="offset", requirements="\d+", nullable=true,
     *      description="List query will only return 100 memes at time."
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function popularMemesAction(ParamFetcherInterface $paramFetcher){

        $em = $this->getDoctrine()->getManager();
        $offset = $paramFetcher->get('offset');

        if(!$offset) {
            $offset = 0;
        }


        $memes = $em->getRepository('AppBundle:Meme')->findPopular(100,$offset);

        $data = array(
            'count'             => count($memes),
            'offset'            => (int)$offset,
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'meme_path'         => '/meme/show/',
            'memes'             => $memes,
        );

        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }
    
}