<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 23.5.2017
 * Time: 14:16
 */

namespace AppBundle\Api;


use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Repository\TemplateRepository;

class TemplateController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get template by ID",
     * )
     *
     * @Annotations\QueryParam(name="id", requirements="\d+", nullable=true, description="Memes id, integer")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function getTemplateAction(ParamFetcherInterface $paramFetcher){
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $id = $paramFetcher->get('id');
        if(!$id){
            $data = array('remember set id.');
            return $this->view($data, 200);
        }

        // query template
        $template = $em->getRepository('AppBundle:Template')->findOneBy(array('id'=>$id));

        $data = array(
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'templates'         => $template,
        );

        // return template
        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get popular templates, max 100, no params",
     * )
     *
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function popularTemplateAction(ParamFetcherInterface $paramFetcher){

        $em = $this->getDoctrine()->getManager();
        $templates = $em->getRepository('AppBundle:Template')->findMostUsed(100);

        $data = array(
            'count'             => count($templates),
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'templates'         => $templates,
        );

        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get newest templates",
     * )
     *
     * @Annotations\QueryParam(
     *      name="offset", requirements="\d+", nullable=true,
     *      description="List query will only return 100 at time, use offset to get next 100."
     * )
     * @Annotations\QueryParam(
     *      name="order", requirements="\w+", nullable=true,
     *      description="Order by ASC or DESC, default DESC"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View $view
     */
    public function listTemplatesAction(ParamFetcherInterface $paramFetcher){

        $em = $this->getDoctrine()->getManager();
        $offset = $paramFetcher->get('offset');
        $order = $paramFetcher->get('order');

        if(!$offset) {
            $offset = 0;
        }

        if($order && $order == 'ASC') {
            $templates = $em->getRepository('AppBundle:Template')->findTemplates(100,$offset);
        } else {
            $templates = $em->getRepository('AppBundle:Template')->findLatest(100,$offset);
        }


        $data = array(
            'count'             => count($templates),
            'offset'            => (int)$offset,
            'image_path'        => '/media/cache/resolve/meme/',
            'image_list_path'   => '/media/cache/resolve/list_thumb/',
            'templates'             => $templates,
        );


        $view = $this->view($data, 200);

        $context = $view->getContext();
        $context->setGroups(array("api"));
        $view->setContext($context);

        return $view;
    }


    private function resolveImage($templates){

        $imagine = $this->get('liip_imagine.cache.manager');

        foreach ($templates as $template) {
            $image = $template->getImage();
            $imagine->resolve($image->getImageName(), 'meme');
            $imagine->resolve($image->getImageName(), 'list_thumb');
        }

    }
}