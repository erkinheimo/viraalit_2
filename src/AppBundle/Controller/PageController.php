<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 29.11.2017
 * Time: 18:18
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\Template;
use Symfony\Component\Yaml\Yaml;
use AppBundle\Entity\Page;




class PageController extends Controller
{
    /**
     * @Route("/{name}")
     */
    function indexAction($name){

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Page $page */
        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('url' => $name));

        if(!$page){ // case no page was found
            throw $this->createNotFoundException('The page does not exist.');
        }
        if(!$page->getPublic()){ // case we won't like to show it yet.
            throw $this->createNotFoundException('The page does not exist.');
        }


        return $this->render('default/page.html.twig', [
            'page' => $page,
        ]);
    }

}

