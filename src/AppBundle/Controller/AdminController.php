<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 27.4.2017
 * Time: 15:34
 */

namespace AppBundle\Controller;



use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use AppBundle\Entity\Image;
use AppBundle\Entity\Template;
use AppBundle\Entity\Meme;
use Symfony\Component\Config\Definition\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class AdminController extends BaseAdminController {

    /**
     * @Route("/api", name="admin_api")
     *
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function apiAction(Request $request){
        return $this->render('easy_admin/api.html.twig', []);
    }

    /**
     * @Route("/index", name="admin_index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminIndexAction(Request $request)
    {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Page $page */
        $daily = $em->getRepository('AppBundle:Meme')->memesPerDay();

        return $this->render('easy_admin/index.html.twig', [
            'days' => $daily,
        ]);
    }

    public function view_imageAction()
    {

        $imagine = $this->container->get('liip_imagine.controller');

        $id = $this->request->query->get('id');
        $entityName = $this->request->query->get('entity');

        $image = $this->em->getRepository('AppBundle:'.$entityName)->find($id);

        if(!$image instanceof Image) {
            $image = $image->getImage();
        }


        if($image instanceof Image) {
            return $imagine->filterAction(
                    new Request(),
                    $image->getRelativePath(),
                    'meme'
                );
        }

        throw $this->createNotFoundException('Image not found.');


    }


    public function view_memeAction()
    {
        $id = $this->request->query->get('id');
        $meme = $this->em->getRepository('AppBundle:Meme')->find($id);

        if($meme instanceof Meme) {
            return $this->redirectToRoute('meme_show', array('meme_url' => $meme->getUrl()));
        }

        throw $this->createNotFoundException('Meme image not found.');
    }

    public function add_as_templateAction()
    {

        $type = $this->request->query->get('entity');

        if($type == 'AdminImage') {
            $admin_image_id = $this->request->query->get('id');
            $admin_image = $this->em->getRepository('AppBundle:AdminImage')->find($admin_image_id);
            $image = $admin_image->getImage();
        } elseif ($type == 'UserImage') {
            $user_image_id = $this->request->query->get('id');
            $user_image = $this->em->getRepository('AppBundle:UserImage')->find($user_image_id);
            $image = $user_image->getImage();
        } elseif ($type == 'Image') {
            $id = $this->request->query->get('id');
            $image = $this->em->getRepository('AppBundle:Image')->find($id);
        }

        if(!$image) {
            throw $this->createNotFoundException('Image not found.');
        }

        $inTemplates = $this->em->getRepository('AppBundle:Template')->findBy(array('image' => $image));


        if($image && !$inTemplates) {
            $template = new Template();
            $template->setImage($image);

            $this->em->persist($template);
            $this->em->flush();

        }

        $refererUrl = $this->request->query->get('referer', '');


        /*
         * Took this return from EasyAdminBundle/AdminController Delete action.
         * Meaning this action doesn't do anything to images so we would like to return to image list.
         */
        return !empty($refererUrl)
            ? $this->redirect(urldecode($refererUrl))
            : $this->redirect($this->generateUrl('easyadmin', array('action' => 'list', 'entity' => $this->entity['name'])));
    }
}