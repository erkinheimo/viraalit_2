<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\Template;
use Symfony\Component\Yaml\Yaml;
use AppBundle\Entity\Page;


/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'index'));
        $popular_templates = $em->getRepository('AppBundle:Template')->findMostUsed(20);
        $templates =         $em->getRepository('AppBundle:Template')->findLatest(40);



        return $this->render('default/index.html.twig', [
            'templates' => $templates,
            'popular_templates' => $popular_templates,
            'page' => $page,
        ]);
    }

    /**
     * @Route("/suomi100v", name="top_meme_sites")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function topMemeSitesAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'top_meme_sites'));

        return $this->render('default/top_meme_sites.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @Route("/about", name="about")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function aboutAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Page $page */
        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'about'));

        return $this->render('default/top_meme_sites.html.twig', [
            'page' => $page,
        ]);
    }
}
