<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use Nietonfir\Google\ReCaptchaBundle\Controller\ReCaptchaValidationInterface;

use AppBundle\Entity\Meme;
use AppBundle\Form\MemeType;
use AppBundle\Entity\Template;
use AppBundle\Entity\UserImage;
use AppBundle\Repository\MemeRepository;


class MemeController extends Controller implements ReCaptchaValidationInterface
{
    /**
     * @Route("/meme/show/{meme_url}", name="meme_show")
     */
    public function showAction(Request $request, $meme_url)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var meme $meme */
        $meme = $em->getRepository('AppBundle:Meme')->findOneBy(array('url'=>$meme_url));
        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'meme_view_add_block'));

        $session = $request->getSession();

        if(!$session->has('viewed_memes')){
            $session->set('viewed_memes', []);
        }
        $viewedMemes = $session->get('viewed_memes');


        if (!$meme) {
            throw $this->createNotFoundException('The meme does not exist');
        }

        if(!in_array($meme->getId(), $viewedMemes)){
            $meme->addView();

            $em->persist($meme);
            $em->flush();

            array_push($viewedMemes,$meme->getId());
            if(count($viewedMemes) > 100){
                array_shift($viewedMemes);
            }
            $session->set('viewed_memes', $viewedMemes);
        }

        return $this->render('meme/show.html.twig', array(
            'meme' => $meme,
            'page' => $page,
        ));

    }

    /**
     * @Route("/meme/create/{image_id}", name = "meme_new")
     */
    public function newAction(Request $request, $image_id)
    {
        $meme = new Meme();
        $em = $this->getDoctrine()->getManager();
        $imageRepo = $this->getImageRepo();
        $image = $imageRepo->findOneBy(array('id' => $image_id));

        $form = $this->createForm(MemeType::class, $meme);


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Meme $meme */
            $meme = $form->getData();
            if(!$meme->getImage()->getImageFile()){
                $meme->setImage($image);
            } else {
                $userimage = new UserImage();
                $userimage->setImage($meme->getImage());
                $em->persist($userimage);
            }

            $meme->createUrl();
            $em->persist($meme);
            $em->flush();

            return $this->redirectToRoute('meme_show', array('meme_url' => $meme->getUrl()));
        } else {
            if(!$form->isSubmitted()) {
                /** set hidden field image_id */
                $form->get('image_id')->setData($image_id);
            }
        }

        $image = $imageRepo->findOneBy(array('id' => $image_id));

        if(!$image) {
            throw $this->createNotFoundException('Meme image not found.');
        }

        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'meme_view_add_block'));

        return $this->render('meme/create.html.twig', array(
            'form' => $form->createView(),
            'image' => $image,
            'page' => $page,
        ));
    }

    /**
     * @Route("/meme/popular", name="meme_popular_list")
     */
    public function popularAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $memes  = $em->getRepository('AppBundle:Meme')->findPopular(40);
        $page   = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'meme_view_add_block'));

        return $this->render('meme/popular.html.twig', array(
            'memes' => $memes,
            'page' => $page,
        ));
    }

    /**
     * @Route("/meme/latest", name="meme_latest_list")
     */
    public function latestAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $memes  = $em->getRepository('AppBundle:Meme')->findLatest(40);
        $page   = $em->getRepository('AppBundle:Page')->findOneBy(array('name' => 'meme_view_add_block'));

        return $this->render('meme/new.html.twig', array(
            'memes' => $memes,
            'page' => $page,
        ));
    }

    /**
     * @return EntityRepository
     */
    private function getImageRepo(){
        return $this->getDoctrine()->getManager()->getRepository('AppBundle:Image');
    }
}
