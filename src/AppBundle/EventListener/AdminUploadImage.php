<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 6.5.2017
 * Time: 21:03
 */

namespace AppBundle\EventListener;


use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\GenericEvent;


use AppBundle\Entity\Image;
use AppBundle\Entity\AdminImage;

class AdminUploadImage {

    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function onEasyadminPostpersist(GenericEvent $event){


        /** If subject is image => admin saved image in admin panel */
        if($event->getSubject() instanceof Image){
            $admin_image = new AdminImage();
            $admin_image->setImage($event->getSubject());

            $this->em->persist($admin_image);
            $this->em->flush();
        }
    }
}