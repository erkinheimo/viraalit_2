<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 24.4.2017
 * Time: 22:27
 */

namespace AppBundle\Service;


use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use AppBundle\Entity\Image;

class UploadNamer implements NamerInterface {

    /**
     * Creates a name for the file being uploaded.
     *
     * @param Image          $object  The object the upload is attached to
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object
     *
     * @return string The file name
     */
    public function name($object, PropertyMapping $mapping) {
        $new_name =   $this->idGen($object->getImageFile()->getFilename() . (string)$object->getUpdatedAt()->getTimestamp());
        $extension =  $object->getImageFile()->guessExtension();
        return $new_name . '.' . $extension;
    }

    private function idGen($str){
        $chars = str_split($str);
        $i = 0;
        $rand_int = 0;
        foreach($chars as $char){
            $i++;
            $rand_int += ord($char);
            if($i > 100){
                break;
            }
        }
        mt_srand($rand_int);
        return mt_rand(100000,PHP_INT_MAX);
    }

}