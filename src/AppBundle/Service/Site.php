<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 2.5.2017
 * Time: 18:20
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

class Site {
    protected $em;

    protected $site;

    public function __construct(EntityManager $em) {
      $this->em = $em;

      $this->site = $em->getRepository('AppBundle:Site')->findAll();
    }


    public function site(){
        return $this->site[0];
    }



}