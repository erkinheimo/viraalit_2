/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    //   /home/erkki/Documents/projects/viraalit_2/src/AppBundle/Resources/public/ckeditor/plugins/

    CKEDITOR.config.allowedContent = true;

    config.extraPlugins = 'justify,basewidget,widget,lineutils,widgetselection';

    config.contentsCss = [ '/css/bootstrap.min.css', '/css/custom.css' ];
};