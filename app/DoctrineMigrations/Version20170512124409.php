<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170512124409 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

//        $this->addSql('DROP TABLE fos_group');
//        $this->addSql('DROP TABLE fos_user_user_group');
        $this->addSql('DROP INDEX UNIQ_957A6479C05FB297');
        $this->addSql('DROP INDEX UNIQ_957A6479A0D96FBF');
        $this->addSql('DROP INDEX UNIQ_957A647992FC23A8');
        $this->addSql('CREATE TEMPORARY TABLE __temp__fos_user AS SELECT id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles FROM fos_user');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('CREATE TABLE fos_user (id INTEGER NOT NULL, username VARCHAR(180) NOT NULL COLLATE BINARY, username_canonical VARCHAR(180) NOT NULL COLLATE BINARY, email VARCHAR(180) NOT NULL COLLATE BINARY, email_canonical VARCHAR(180) NOT NULL COLLATE BINARY, enabled BOOLEAN NOT NULL, salt VARCHAR(255) DEFAULT NULL COLLATE BINARY, password VARCHAR(255) NOT NULL COLLATE BINARY, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL COLLATE BINARY, password_requested_at DATETIME DEFAULT NULL, roles CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO fos_user (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) SELECT id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles FROM __temp__fos_user');
        $this->addSql('DROP TABLE __temp__fos_user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A6479C05FB297 ON fos_user (confirmation_token)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A6479A0D96FBF ON fos_user (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A647992FC23A8 ON fos_user (username_canonical)');
        $this->addSql('DROP INDEX IDX_4B9F79343DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__meme AS SELECT id, image_id, updated_at, toptext, bottomtext, url, viewcount FROM meme');
        $this->addSql('DROP TABLE meme');
        $this->addSql('CREATE TABLE meme (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, toptext VARCHAR(255) DEFAULT NULL COLLATE BINARY, bottomtext VARCHAR(255) DEFAULT NULL COLLATE BINARY, url VARCHAR(255) NOT NULL COLLATE BINARY, viewcount INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_4B9F79343DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO meme (id, image_id, updated_at, toptext, bottomtext, url, viewcount) SELECT id, image_id, updated_at, toptext, bottomtext, url, viewcount FROM __temp__meme');
        $this->addSql('DROP TABLE __temp__meme');
        $this->addSql('CREATE INDEX IDX_4B9F79343DA5256D ON meme (image_id)');
        $this->addSql('DROP INDEX UNIQ_97601F833DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__template AS SELECT id, image_id, updated_at FROM template');
        $this->addSql('DROP TABLE template');
        $this->addSql('CREATE TABLE template (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_97601F833DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO template (id, image_id, updated_at) SELECT id, image_id, updated_at FROM __temp__template');
        $this->addSql('DROP TABLE __temp__template');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_97601F833DA5256D ON template (image_id)');
        $this->addSql('DROP INDEX IDX_77EDFB833DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_template AS SELECT id, image_id, updated_at FROM user_template');
        $this->addSql('DROP TABLE user_template');
        $this->addSql('CREATE TABLE user_template (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_77EDFB833DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user_template (id, image_id, updated_at) SELECT id, image_id, updated_at FROM __temp__user_template');
        $this->addSql('DROP TABLE __temp__user_template');
        $this->addSql('CREATE INDEX IDX_77EDFB833DA5256D ON user_template (image_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE fos_group (id INTEGER NOT NULL, name VARCHAR(180) NOT NULL COLLATE BINARY, roles CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4B019DDB5E237E06 ON fos_group (name)');
        $this->addSql('CREATE TABLE fos_user_user_group (user_id INTEGER NOT NULL, group_id INTEGER NOT NULL, PRIMARY KEY(user_id, group_id))');
        $this->addSql('CREATE INDEX IDX_B3C77447FE54D947 ON fos_user_user_group (group_id)');
        $this->addSql('CREATE INDEX IDX_B3C77447A76ED395 ON fos_user_user_group (user_id)');
        $this->addSql('DROP INDEX UNIQ_957A647992FC23A8');
        $this->addSql('DROP INDEX UNIQ_957A6479A0D96FBF');
        $this->addSql('DROP INDEX UNIQ_957A6479C05FB297');
        $this->addSql('CREATE TEMPORARY TABLE __temp__fos_user AS SELECT id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles FROM fos_user');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('CREATE TABLE fos_user (id INTEGER NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO fos_user (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) SELECT id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles FROM __temp__fos_user');
        $this->addSql('DROP TABLE __temp__fos_user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A647992FC23A8 ON fos_user (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A6479A0D96FBF ON fos_user (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A6479C05FB297 ON fos_user (confirmation_token)');
        $this->addSql('DROP INDEX IDX_4B9F79343DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__meme AS SELECT id, image_id, updated_at, toptext, bottomtext, url, viewcount FROM meme');
        $this->addSql('DROP TABLE meme');
        $this->addSql('CREATE TABLE meme (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, toptext VARCHAR(255) DEFAULT NULL, bottomtext VARCHAR(255) DEFAULT NULL, url VARCHAR(255) NOT NULL, viewcount INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO meme (id, image_id, updated_at, toptext, bottomtext, url, viewcount) SELECT id, image_id, updated_at, toptext, bottomtext, url, viewcount FROM __temp__meme');
        $this->addSql('DROP TABLE __temp__meme');
        $this->addSql('CREATE INDEX IDX_4B9F79343DA5256D ON meme (image_id)');
        $this->addSql('DROP INDEX UNIQ_97601F833DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__template AS SELECT id, image_id, updated_at FROM template');
        $this->addSql('DROP TABLE template');
        $this->addSql('CREATE TABLE template (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO template (id, image_id, updated_at) SELECT id, image_id, updated_at FROM __temp__template');
        $this->addSql('DROP TABLE __temp__template');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_97601F833DA5256D ON template (image_id)');
        $this->addSql('DROP INDEX IDX_77EDFB833DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_template AS SELECT id, image_id, updated_at FROM user_template');
        $this->addSql('DROP TABLE user_template');
        $this->addSql('CREATE TABLE user_template (id INTEGER NOT NULL, image_id INTEGER DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user_template (id, image_id, updated_at) SELECT id, image_id, updated_at FROM __temp__user_template');
        $this->addSql('DROP TABLE __temp__user_template');
        $this->addSql('CREATE INDEX IDX_77EDFB833DA5256D ON user_template (image_id)');
    }
}
