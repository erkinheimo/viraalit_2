<?php
/**
 * Created by PhpStorm.
 * User: erkki
 * Date: 18.5.2017
 * Time: 15:56
 */

namespace tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url){

//        $url = $this->urlProvider();

        $client = self::createClient();

        $client->request('GET', $url);

        $this->assertTrue( $client->getResponse()->isSuccessful() );
    }

    public function urlProvider()
    {
        return array(
            array('/'),
            array('/about'),
            array('/suomi100v'),
            array('/meme/popular'),
            array('/meme/latest'),
        );
    }


}